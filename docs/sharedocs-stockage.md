description: Présentation, fonctionnement et utilisation du gestionnaire de fichiers ShareDocs


# ShareDocs - Présentation, fonctionnement et utilisation

## Présentation et conditions d’accès à ShareDocs

### Qu’est-ce que ShareDocs

ShareDocs est un gestionnaire de fichiers mis en oeuvre par la TGIR Huma-Num, sur ses propres serveurs, pouvant être utilisé via un navigateur web, un client WebDAV ou un logiciel de synchronisation de fichiers. La plateforme est basée sur l’application FileRun : [Présentation générale](https://filerun.com/) et [Manuel d’utilisation des opérations de base](https://docs.filerun.com/user_guide). Il s’agit d’un stockage déporté en ligne sur le réseau recherche français (semblable à des outils comme DropBox hébergés eux sur le cloud public). Les données hébergées peuvent être cryptées de manière simple grâce à des outils comme [ZED encrypt](https://www.zedencrypt.com/download).

Il est adapté aux projets de recherche qui souhaitent stocker, échanger, partager, travailler sur des données de type fichiers (ensemble de photos, de textes transcrits, etc). Ainsi, l’usage courant de ShareDocs permet de préparer des fichiers pour une édition en ligne ou bien une diffusion dans [NAKALA](https://www.nakala.fr/) (par exemple).

### Demander un compte ShareDocs

Pour demander un compte Sharedocs, rendez-vous [sur HumanID](https://humanid.huma-num.fr/) le service d’authentification centralisé d’Huma-Num. Si vous avez un compte HumanID, vous pouvez demander l’accès directement :

![Demande d'accès](media/sharedoc-acces.png){: style="margin-left:100px;"}

Si vous n’avez pas de compte HumanID : vous devez créer un compte sur HumanID pour demander l’accès ([voir la documentation](humanid.md)).

L’ouverture du compte requiert les informations suivantes :
- Établissement de rattachement
- Statut
- Courte description du projet
- Volume estimé des données à héberger

## Accès et arborescence des contenus stockés

ShareDocs peut s’utiliser avec un navigateur en ligne (via [sharedocs.huma-num.fr](https://sharedocs.huma-num.fr/)) ou en synchronisant un répertoire de son ordinateur avec un répertoire ShareDocs stocké chez Huma-Num (avec un logiciel de synchronisation de fichiers comme NextCloud ou encore avec tout autre logiciel client WebDAV permettant la synchronisation de fichiers).

L’organisation des dossiers et documents se fait suivant le principe de l’arborescence, visible sur la gauche.

## Synchronisation de stockage par WebDAV

### Clients de synchronisation WebDAV

Il existe plusieurs clients WebDAV qui permettent d’utiliser le stockage via un client sur son ordinateur. En client opensource, multiplateforme, on peut citer [Cyberduck](https://cyberduck.io) assez simple d’utilisation. Cyberduck dispose d’une fonction dite de coffre-fort, c’est-à-dire qu’il peut crypter les fichiers et les dossiers avant l’envoi sur le serveur. Les fichiers resteront donc cryptés sur l’espace de stockage de la TGIR car la clé de cryptage est propre à l’utilisateur. L’éditeur de Cyberduck propose aussi un client de synchronisation ([mountainduck](https://mountainduck.io/)) qui présente le stockage comme un disque connecté à l’ordinateur. Ce client est payant et reprend aussi les fonctions de cryptage de fichier. Pour des opérations de synchronisation récurrentes, on peut citer le logiciel commercial [GoodSync](https://www.goodsync.com/) qui a l’avantage de rendre visible via un rapport le sens de la synchronisation (son poste vers le serveur ou l’inverse) et l’état des fichiers qui vont être écrasés.

### Authentification WebDAV

L’authentification WebDav nécéssite de générer une clé d’authentification à utiliser dans le client. Pour générer la clé :

- Rendez-vous sur Paramètres de compte (activer le menu sous le bouton coloré composé de ses initiales en haut à droite de l’écran)

![Paramètre Sharedocs](media/sharedoc-parametre.png){: style="border: 1px solid grey"}

- Dans la fenêtre qui s’affiche, cliquez sur le bouton [Connect WebDAV app]
- Dans la fenêtre d’Autorisation, cliquez sur [Acceptez]


![Sharedocs Autorisation](media/sharedoc-autorisation.png){: style="border: 1px solid grey"}

- Une fenêtre WebDAV Credentials vous donne les informations suivantes : URL de connection, nom d’utilisateur, mot de passe (la clé d’authentification)

Indiquez ces informations dans votre client WebDAV pour vous connecter par ce moyen. Pensez à sauvegarder ces informations, une fois générée la clé n’est plus visible dans l’interface

### Révoquer l’accès WebDAV

- Pour révoquer l’accès par WebDAV vous devez invalider la clé d’authentification, pour cela retournez dans le menu "Paramètres du compte" puis révoquez l’accès : cocher la case [Révoquer l’accès] puis enregistrer.

![Sharedocs Révocation](media/sharedoc-revocation.png){: style="border: 1px solid grey"}

## Affichage et édition des fichiers

Pour chaque fichier, des méthodes de visualisation et/ou d’édition sont accessibles pour certains formats, via le menu "ouvrir avec" (faire clic droit sur un nom de fichier, ou utiliser l’icône autre option en haut à gauche)

![Sharedocs Ouvrir avec](media/sharedoc-openwith.png){: style="border: 1px solid grey"}

Par exemple, pour les documents Word (`.doc` et `.docx`), il est possible :

- soit de visualiser le contenu via ouvrir avec &rarr; Office web Viewer
- soit de l’éditer via ouvrir avec &rarr; Only Office

!!! Warning "Attention"
    Les fonctions disponibles dans "ouvrir avec" envoient le fichier, pour certaines d’entre elles, sur des serveurs qui sont externes à la TGIR. Pour les projets qui ont des contraintes légales sur l’usage et le transfert des fichiers, ils ne doivent pas utiliser ces services qui transfèrent les données hors de la TGIR (généralement aux États-Unis).

Les fonctions d’édition où les données restent internes aux serveurs de la TGIR sont :

- OnlyOffice (édition de document Office)
- Éditeur de texte
- Lecteur de PDF
- Lecteur Audio/Vidéo
- Visualisation d’image
- Explorateur d’archive
- Open document Viewer
- Information sur le média
- Créer une épreuve photo

Les autres (actuellement Office web Viewer) transfèrent le fichier sur des serveurs externes, avant d’être enregistrés de nouveau dans ShareDocs.

!!! Warning "Attention"
    Mes métadonnées entrées sur les fichiers sont internes à l’application ShareDocs et ne peuvent être exportées.

## Versionnage

Dans le menu contextuel (clic droit sur le fichier) : `Autres options > Version > Versions précédentes` donne la possibilité de télécharger une ancienne version, ou de la restaurer.

La restauration crée une nouvelle version sans effacer l’historique, mais n’indique pas que la nouvelle version est une restauration.

![Sharedocs Versionnage](media/sharedoc-versioning.png){: style="border: 1px solid grey"}

## Questions et Contact

Pour tout problème ou question liée à l’utilisation de Sharedocs et des outils mentionnés, veuillez envoyer un mail à l’adresse suivante : [assistance@huma-num.fr](mailto:assistance@huma-num.fr).
