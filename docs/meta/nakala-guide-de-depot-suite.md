---
lang: fr
---

!!! Note  
    Document en cours de rédaction


# Guide pour décrire ses données dans NAKALA (suite)

----------

### Les éléments calculés automatiquement par NAKALA

#### Les noms des fichiers déposés (nakala:?)
#### Les tailles des fichiers déposés (nakala:?)
#### les formats des fichiers déposés (nakala:?)
#### Les empreintes des fichiers déposés (nakala:?)
#### La date de la soumission (nakala:?)
#### l'identifiant du déposant (nakala:?)
#### l'identifiant (DOI) du dépôt (nakala:?)




----------

### Les métadonnées complémentaires (vocabulaire Dublin-Core)

#### dcterms:creator
```
Quelle utilité puisque nakala:creator a déjà une cardinalité multiple?
```
#### dcterms:title
```
Quelle utilité puisque nakala:title a déjà une cardinalité multiple?
```
#### dcterms:created
```
Faut-il garder une dcterms:created complémentaire de nakala:created?
```
#### dcterms:license
```
Faut-il garder une dcterms:license complémentaire de nakala:license?
```
#### dcterms:type
```
La nature ou le genre de la ressource. Complémentaire de nakala:type
```

#### dcterms:available
Date à laquelle la ressource est ou deviendra disponible.

```
On devrait plutôt gérer une propriété de ce sens par NAKALA (nakala:available)
qui aurait pour valeur la date de publication ou la date cible d'après embargo si embargo déclaré.

```

#### dcterms:abstract
Résumé du contenu de la ressource sous forme de texte libre. 
Préciser la langue du résumé.

#### dcterms:accessRights
Information sur les conditions d'accès à la ressource ou sur son statut en terme de sécurité.
Par exemple: "Librement accessible" ou "Soumis à embargo en raison de..."
Précisez la langue du contenu du champ.

#### dcterms:alternative
Titre secondaire, titre abrégé ou autre nom donné à la ressource.
Précisez la langue du contenu du champ. 

#### dcterms:audience
Type de population pour laquelle la ressource est destinée ou utile.
Précisez la langue du contenu du champ.

#### dcterms:bibliographicCitation
Une référence bibliographique pour la ressource.
Précisez la langue du contenu du champ.

#### dcterms:conformsTo
Le standard, la norme ou les spécifications formelles auxquels la ressource décrite est conforme.
Si possible, indiquez son identifiant (DOI, ISBN...), une forme de citation, son nom ou une description.
Si besoin indiquez la langue du contenu du champ.

#### dcterms:contributor
Entité ayant contribué à l'élaboration du contenu de la ressource (individu, institution, organisation, service),
autre que l'entité identifiée dans nakala:creator ou alors en précisant la nature de sa contribution.

```
Que dire sur la forme du contenu? URI ORCID, VIAF, ISNI.... 
Forme normalisée () Nom, Prénom, (dates extrêmes), rôle

```

#### dcterms:coverage
Portée, couverture de la ressource.
S'il s'agit d'une portée ou couverture géographique utiliser plutôt dcterms:spatial. 
S'il s'agit d'une portée ou couverture temporelle utiliser plutôt dcterms:temporel. 
Précisez la langue du contenu du champ.


#### dcterms:date
Date dans le cycle de vie de la ressource. Privilégiez plutôt, quand c'est possible, l'utilisation de termes
plus précis (available, created, dateAccepted, dateCopyrighted, dateSubmitted, issued, modified, valid)

Lorsque c'est possible, utilisez la syntaxe W3CDTF en précisant l'encodage dcterms:W3CDTF. Cf. 
[https://www.w3.org/TR/NOTE-datetime](https://www.w3.org/TR/NOTE-datetime) pour les différents formats acceptables:
année, année+mois, année+mois+jour, année+mois+jour+heure+minutes+secondes  (eg "1997-07-16T19:20:30+01:00")

Il est possible également de définir une période en précisant le type dcterms:Period. Cf 
[https://www.dublincore.org/specifications/dublin-core/dcmi-period/](https://www.dublincore.org/specifications/dublin-core/dcmi-period/)
pour les différents formats acceptables (eg "name=The Great Depression; start=1929; end=1939;")

#### dcterms:description
Description du contenu de la ressource sous forme de texte libre. 
Préciser la langue de la description.
Si la description est un résumé du contenu utiliser plutôt dcterms:abstract. 
Si la description est une table des matières utiliser plutôt dcterms:tableOfContents.

#### dcterms:educationLevel
Niveau d'éducation du public auquel la ressource est destinée.
Préciser la langue de la description.

#### dcterms:extent
La taille ou la durée de la ressource.

Pour les durées, utilisez quand c'est possible un encodage xsd:duration exprimée sous forme d'une chaîne
de caractères sur le modèle de PnYnMnDTnHnMnS (eg pour une durée de 2 heures 15 minutes PT2H15S). Cf les 
spécifications "XML Schema Part 2: Datatypes" sur le site du W3C
[https://www.w3.org/TR/](https://www.w3.org/TR/) pour plus de précisions.

Pour les tailles indiquez l'unité (eg "120 pages"). Ne pas indiquer la taille en octets du fichier qui est déjà exprimée
dans la propriété nakala:?

#### dcterms:format
Le format de fichier, le support physique ou les dimensions de la ressource.

Eg "in-quarto à 2 col"

#### dcterms:identifier
Identifiant, cote de la ressource décrite. 
Ne pas y mettre l'identifiant DOI qui est déjà exprimé dans la propriété nakala:?
#### dcterms:instructionalMethod
Un processus utilisé pour engendrer des connaissances, des attitudes et des compétences,
pour le soutien duquel la ressource décrite est prévue.
#### dcterms:language
La langue de la ressource.
#### dcterms:mediator
Une entité qui modère l'accès à la ressource et à qui la ressource est destinée ou utile.
#### dcterms:medium
La matière ou le support physique de la ressource.
#### dcterms:provenance
Une déclaration de tous les changements d'appartenance et de garde de la ressource depuis sa création qui sont importants pour son authenticité, son intégrité et son interprétation.
#### dcterms:publisher
Une entité responsable de la mise à disposition de la ressource.
#### dcterms:relation
Une ressource liée.
Privilégiez, quand c'est possible les termes plus spécifiques (conformsTo, hasFormat, hasPart, hasVersion, 
isFormatOf, isPartOf, isReferencedBy, isReplacedBy, isRequiredBy, isVersionOf, references, replaces, requires
#### dcterms:rights
Une information à propos des droits détenus dans et sur la ressource.
#### dcterms:rightsHolder
Une personne ou une organisation possédant ou exploitant les droits sur la ressources.
#### dcterms:source
Une ressource liée de laquelle dérive la ressource décrite.
#### dcterms:spatial
La couverture ou portée spatiale de la ressource.
#### dcterms:subject
Le sujet de la ressource.
#### dcterms:tableOfContents
Table des matière du contenu de la ressource sous forme de texte libre. 
Préciser la langue de la table des matière.


```
Conseille-t-on de structurer cette table des matières avec une syntaxe de type RichText?

```


#### dcterms:temporal
Couverture temporelle de la ressource

### Les métadonnées expertes (vocabulaire Dublin-Core)

#### Métadonnées propres aux collections
A n'utiliser que pour les métadonnées des collections.

| propriété | définition |
|--|--|
| **dcterms:accrualMethod** | Méthode par laquelle les ressources peuvent être ajoutées dans une collection. Utilisez si possible le vocabulaire "Collection Description Accrual Method Vocabulary" <https://www.dublincore.org/specifications/dublin-core/collection-description/accrual-method/>. |
| **dcterms:accrualPeriodicity** | Fréquence avec laquelle les ressources sont ajoutées dans une collection. Utilisez si possible le vocabulaire "Collection Description Frequency Vocabulary" <https://www.dublincore.org/specifications/dublin-core/collection-description/frequency/>. |
| **dcterms:accrualPolicy** | Politique qui gouverne l'ajout de ressources dans une collection. Utilisez si possible le vocabulaire "Collection Description Accrual Policy Vocabulary" <https://www.dublincore.org/specifications/dublin-core/collection-description/accrual-policy/>. |

#### Métadonnées de dates spécialisées

Lorsque c'est possible, utilisez la syntaxe W3CDTF en le précisant dans l'encodage. Cf. 
[https://www.w3.org/TR/NOTE-datetime](https://www.w3.org/TR/NOTE-datetime) pour les différents formats acceptables:
année, année+mois, année+mois+jour, année+mois+jour+heure+minutes+secondes  (eg "1997-07-16T19:20:30+01:00")

Il est possible également de définir une période en précisant le type dcterms:Period. Cf 
[https://www.dublincore.org/specifications/dublin-core/dcmi-period/](https://www.dublincore.org/specifications/dublin-core/dcmi-period/)
pour les différents formats acceptables (eg "name=The Great Depression; start=1929; end=1939;")

| date | définition |
|--|--|
| **dcterms:dateAccepted**    | Date d'acceptation de la ressource. |
| **dcterms:dateCopyrighted** | Date de dépôt des droits d'auteur. |
| **dcterms:dateSubmitted**   | Date de soumission de la ressource.|
| **dcterms:issued**          | Date de parution formelle (par exemple, la publication) de la ressource.|
| **dcterms:modified**        | Date à laquelle la ressource a été modifiée.|
| **dcterms:valid**           | Date (souvent un intervalle) de validité d'une ressource.|

#### Métadonnées de relations spécialisées

| relation | définition |
|--|--|
| **dcterms:replaces**       | Une ressource liée qui est supplantée, déplacée ou remplacée par la ressource décrite. |
| **dcterms:isReplacedBy**   | Une ressource liée qui supplante, déplace ou remplace la ressource décrite. |
| **dcterms:requires**       | Une ressource liée qui est exigée par la ressource décrite pour soutenir sa fonction, sa distribution ou sa cohérence. |
| **dcterms:isRequiredBy**   | Une ressource liée qui exige de la ressource décrite qu'elle soutienne sa fonction, sa distribution ou sa cohérence. |
| **dcterms:hasVersion**     | Une ressource liée qui est une version, une édition ou une adaptation de la ressource décrite. |
| **dcterms:isVersionOf**    | Une ressource liée dont la ressource décrite est une version, une édition ou une adaptation. |
| **dcterms:hasFormat**      | Une ressource liée qui est en substance la même que la ressource décrite préexistente mais dans un autre format. |
| **dcterms:isFormatOf**     | Une ressource liée qui est en substance la même que la ressource décrite mais dans un autre format. |
| **dcterms:hasPart**        | Une ressource liée qui est incluse physiquement ou logiquement dans la ressource décrite. |
| **dcterms:isPartOf**       | Une ressource liée dans laquelle la ressource décrite est physiquement ou logiquement incluse. |
| **dcterms:references**     | Une ressource liée qui est référencée, citée ou encore vers laquelle pointe la ressource décrite. |
| **dcterms:isReferencedBy** | Une ressource liée qui référence, cite ou encore pointe vers la ressource décrite. |

