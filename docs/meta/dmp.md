---
lang: fr
---

!!! Note  
    Document en cours de rédaction


# Data Management Plan - DMP 
(Plan de Gestion de données)

Huma-Num vous apporte des outils et moyens à signaler dans la rédaction d’un DMP.

## Rédaction d’un DMP

Le plan de gestion des données (PGD) ou Data Management Plan (DMP) est un outil de gestion. Il se présente sous forme d’un document structuré en rubriques. Il a pour objectif de synthétiser la description et l’évolution des jeux de données de votre projet de recherche.  Il prépare le partage, la réutilisation et la pérennisation des données.

-  [Fiche DMP sur Doranum](https://doranum.fr/plan-gestion-donnees-dmp/) de l’INIST.

Plusieurs acteurs interviennent dans l’accompagnement à la rédaction des DMP. Huma-Num procure des services qui permettent aux responsables de projet de gérer leurs données en cohérence avec les préconisations actuelles et de renseigner un DMP.  

## Appui d’Huma-Num dans la rédaction des DMP

Huma-Num ne rédige pas directement les DMP des projets, elle procure les services nécessaire à la réalisation des actions décrites dans les DMP tout au long du cycle de vie des données de la recherche et accompagne les porteurs de projets dans leur démarche.

Ainsi, Huma-Num procure, exploite et accompagne l’utilisation de services numériques mentionnés dans les DMP :
- Un espace de stockage en ligne sécurisé (ShareDocs)  qui permet une première préservation et un travail collaboratif sur les données collectées     
- Outils de traitement adaptés aux données des projets SHS : OCR, transcodage vidéo et audio, Système d’Information Géographiques, outil de visualisation, outil de traitement de données 3D, serveur IIIF  etc.)
- Pour l’étape suivant lorsque les données sont traitées, Huma-Num développe et opère un entrepôt de données spécifique pour les SHS : NAKALA dont elle accompagne l’utilisation. Cet outil permet l’exposition des données, il met en oeuvre des standards et réalise ainsi les aspects “ AIR” des principes FAIR
- Pour la diffusion de données et publications en SHS, le moteur et assistant de recherche ISIDORE permet de réaliser l’aspect “F” des principes “FAIR”.

## Recommandations pour les SHS

### Modèles existants pour les communautés

- Archéologie : modèle de DMP documenté par le consortium MASA

### Formats de données produites ou collectées

Huma-Num travaille avec le CINES, expert dans l’archivage numérique des formats. On peut se référer à la liste des formats validés par le CINES pour sa plateforme PAC (https://facile.cines.fr/). Le service facile du cines est intégré dans NAKALA (possibilité de vérifier la validité de ses dépôts directement)

Il existe des communautés d’usages autour des formats dont voici des exemples :

- Formats 3D : le Livre blanc du consortium 3D : https://shs3d.hypotheses.org/3594
- Formats audio et vidéos : https://www.huma-num.fr/ressources/guide-formats-numeriques
- Format PDF : guides PDF

Expliquer la séparation de ce qu’il faut faire pour les échanges / la préservation. Pour les échanges voir [le Référentiel Interopérabilité RGI](http://references.modernisation.gouv.fr/sites/default/files/Referentiel_General_Interoperabilite_V2.pdf)


### Cycle de vie des données pendant le projet

#### Services pour la sécurisation des données

Dès le début du projet et tout au long de sa vie, Huma-Num procure, exploite et accompagne l’utilisation d’espaces de stockages sécurisés :
- **ShareDocs** : un gestionnaire de fichiers en ligne (Logiciel FileRun) qui permet une première préservation et un travail collaboratif sur les données collectées. Service hébergé par Huma-Num au centre de Calcul de l’IN2P3, sur ses propres serveurs, pouvant être utilisé via un navigateur web, un client WebDAV ou un logiciel de synchronisation de fichiers. L’ouverture de comptes est simple et rapide.
- **Huma-Num Box** : un espace de stockage déporté (Active Circle) qui permet le stockage de données massives, réparti sur le territoire. L’ouverture de ce espace nécessite un entretien avec l’équipe pour définir les besoins et un minimum d’expertise pour l’accès (sftp). Voir [la documentation complète](humanum-box.md).

#### Services pour la préservation et l’exposition des données

L’entrepôt NAKALA répond au besoin de préservation (données documentées) et à l’exposition des données.

HN : Huma-Num exploite et maintient un entrepôt de données NAKALA qui répond aux exigences de préservation et d’exposition des données
Ici encore le code du patrimoine s’applique. = règles de délais d’embargo etc.

HN : Sur la réutilisation, le choix d’une licence est impératif : les données ne peuvent être considérés comme pleinement ouverts que s’ils sont rendus publics sous les conditions d’une licence dite libre ou ouverte. En effet, un objet diffusé sans licence est certes consultable par les tiers, ce qui constitue une forme minimale d’ouverture, mais ses modalités de réutilisation ne sont pas connues. Le choix final de la licence appartient à ceux qui rendent publics leur production (cf <https://www.ouvrirlascience.fr/wp-content/uploads/2019/11/Avis_GTEvaluation_DGRI_14novembre.pdf>)  
On permet de donner une licence dans NAKALA = C’est ça que propose HN

#### Services pour la diffusion des données

ISIDORE indexe les sources de données SHS et les diffuse (interface web, API etc) permettant leur réutilisation (ajout d’une licence).

### Questions éthiques et juridiques

Recommandations pour définir si le projet va comporter des données soumises à la RGPD :

* Définition d’une donnée personnelle : <https://www.cnil.fr/fr/definition/donnee-personnelle>
* Cas particulier des données de santé : définition d’une donnée de santé (<https://www.cnil.fr/fr/quest-ce-ce-quune-donnee-de-sante>)
* Cas particulier des captations audio et vidéo : prévoir le consentement éclairé des personnes concernées

Prendre contact avec le DPO de son établissement pour l’informer du projet qui débute.
  - Contact CNRS = Déléguée à la protection des données (DPD) <https://intranet.cnrs.fr/protection_donnees/reseaux-de-contact/Pages/i-l.aspx>

Prendre contact avec le Comité éthique. CNRS = <https://comite-ethique.cnrs.fr>/

Code de conduite : prévoir la sensibilisation des participants du projet aux règles d’hygiène numérique (<https://www.ssi.gouv.fr/administration/guide/guide-dhygiene-informatique/>). Pour le responsable du projet et/ou le responsable de la gestion des données (cf 6a), se former aux bases de la gestion des données personnelles (exemple <https://atelier-rgpd.cnil.fr)>. Insérer ce temps de formation dans le planning du projet

Ces éléments font l’objet de recommandations rédigées par le réseau SupDPO (<https://reseau.supdpo.fr/wp-content/uploads/2020/01/SupDPO-Recommandations-chercheurs-v1.pdf>)
