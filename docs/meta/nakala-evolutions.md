---
lang: fr
---

!!! Note  
    Document en cours de rédaction


# Evolution(s) des contenus de NAKALA 

En cas d'évolution, les producteurs de données seront informés directement via la liste de diffusion [dédiée](https://listes.huma-num.fr/wws/info/nakala-community). Si une action ou une décision de leur part est nécessaire, Huma-Num leur fournira toute la documentation et le  support nécessaire.

Les utilisateurs seront informés via les canaux de diffusion habituels de Huma-Num (i.e. Blog, Listes de diffusion etc.).

## Evolution des métadonnées 

L'évolution des métadonnées de NAKALA pourra être rendu nécessaire pour différentes raisons :

- Evolution des referentiels utilisés : lorsque le contenu des référentiels utilisés est mis à jour, cela peut provoquer des évolutions du contenu des métadonnées par exemple si des termes sont modifiés ou disparaissent. De nouveaux référentiels seront recherchés régulièrement pour évaluer la pertinence de leur intégration dans NAKALA. L'intégration de ces nouveaux référentiels pourra permettre un enrichissement des contenus existant et éventuellement nécessiter un alignement avec les termes utilisés précédemment ;   
- Evolution du modèle de métadonnées : Le modèle actuel, le [DublinCore](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/) est un modèle générique. Il est prévu d'ajouter progressivement d'autres modèles de métadonnées en fonction de leur pertinence et des demandes de différentes communautés utilisatrices. De même que pour l'évolution des référentiels, ces évolutions pourront permettre un enrichissment des contenus existant.  
- Reprise à effectuer en cas d'évolution technologique de la plate-forme NAKALA : Les cycles des évolutions technologiques sont de plus en plus courts et imposent des mises à jours fréquentes pour éviter l'obsolescence technologique. Ces évolutions peuvent provoquer dans certains cas des évolutions sur les métadonnées qui seront effectuées dans la mesure du possible de manière automatisée. 
- Reprise à effectuer en cas d'évolution réglementaire : Le contenu des métadonnées est également suscpetible d'évoluer en raison d'un changement de réglementation ou de politique générale. on peut citer par exemple, la mise en oeuvre du réglement RGPD ou l'obligation de préciser une licence. De manière générale le type de modifications des métadonnées provoquées par ces évolutions ne peuvent être traitées directement car cela peut nécessiter une décision : Huma-Num prendra contact avec les producteurs pour les informer et les assister dans ces évolutions. 

## Evolution des données 

Lors des contrôles réguliers effectués par Huma-Num (Cf. [Foire Aux Questions](https://documentation.huma-num.fr/meta/nakala-faq/)), la non conformité du format des données par rapport aux spécifications peut être détectée. Le producteur en sera informé et il lui sera suggéré d'effectuer une correction avec des outils adaptés s'ils sont disponibles. 

L'évolution des données elles mêmes, en particulier leur format peut être rendu nécessaire pour plusieurs motifs :

- Le format est devenu obsolète, par exemple les outils pour le générer et le vérifier ne sont plus maintenus ;  
- Le format n'est plus libre, par exemple son utilisation nécessite le paiement d'une licence ;   
- Le format n'est plus utilisée par aucune communauté ;  
- Le format n'est pas optimal pour son utilisation avec des outils associés à NAKALA (e.g. serveur IIIF) ;  
etc.

Une veille sur les formats est assurée par la participation de membres d'Huma-Num à la [cellule de veille nationale sur les formats](https://www.association-aristote.fr/cellule-format/) de l'association [Aristote](https://www.association-aristote.fr/) ainsi que par des relations directe avec notre opérateur d'archivage [CINES](https://www.cines.fr/archivage/).
