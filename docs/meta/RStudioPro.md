# RStudio Server Pro & RShiny Server Server

## Introduction

La TGIR Huma-Num met à disposition des communautés de recherche en SHS un accès aux logiciels RStudio Server Pro et RShiny Server Pro. 

## Demande de création d’un compte

L’accès au service nécessite de faire une demande d’accès via l'adresse du comité de la grille d'Huma-Num pour que la demande soit étudiée : [cogrid@huma-num.fr](cogrid@huma-num.fr).

## Détails techniques

Les deux services actuels reposent sur un accès à un serveur interactif avec une instance affectée au compte avec par défaut :

- 64 coeurs de calcul.
- 20 Go de RAM GPU (VRAM).
- 28 Streaming Multiprocessor (SM), pouvant être porté à 42 SM suivant la charge.
- 1 To de RAM.

Il faut bien comprendre que le serveur et ses capacités sont partagés entre tous les utilisateurs du service. Un utilisateur ne pourra ainsi pas l’utiliser en entier pour un calcul donné. Les informations de connexion transmises sont par défaut pour RStudio mais permettent aussi de se connecter en SSH sur le système dont son nom réel est le worker02. Ce compte est initialisé avec, par défaut :

- Anaconda 2020.11.
- Python 3.8.
- Un environnement virtuel vide.
- 28 SM et 20Go de GPU.

La version par défault de R est : 4.0.5. Cette version peut cependant être changée dans le menu en haut à droite dans RStudio ou dans les préferences du projet.


## Utilisation du service

Quelques informations sont importantes à noter dans l'utilisation du service : 

- Le serveur n’a pas vocation à faire de la conservation de données. **Il est donc demandé expressément aux utilisateurs de ne pas stocker les données primaires, les modèles entraînés ou les données issues de la recherche une fois les calculs réalisés**.  

- De même, comme c’est un serveur qui est partagé entre plusieurs comptes, **il est interdit de modifier le système d’exploitation, ou les packages du serveur**.

- Pour l'utilisation de Python, la version est de base (sans packages), donc il faut utiliser la procédure usuelle de création d'environnement virtuel pour ensuite gérer vos propres packages.

- La CPU est de l’AMD donc les compilateurs et code optimisés INTEL ne peuvent fonctionner.

- La GPU est une Nvidia Ampere dernière génération ; il faut donc obligatoirement utiliser CUDA 11 et Cudnn 8. Beaucoup de scripts ou packages n’ayant pas été mis à jour pour cette version, il est donc nécessaire de vérifier sur des tests préalables que le programme calcule bien en GPU et ne tourne pas dans le vide (comme c’est le cas par exemple avec tout ce qui est Tensorflow inférieur à la version 2.4) ou en CPU (comme il y a 64 coeurs, on peut avoir l’impression que cela va vite alors qu’il n’y a pas d’accélération par l'utilisation des GPU).

## Comparaison de la solution RStudio open-source et RStudio Pro

Categorie | Description | Edition open-source | Edition Pro
:---: | :---: | :---: | :---:
**Overview** | Access the RStudio IDE from anywhere via a web browser | + | +
x  | Move computation close to the data | + | +
x  | Scale compute and RAM centrally | + | +
x  | Powerful coding tools to enhance your productivity | + | +
x  | Easily publish apps and reports | + | +
**Python Development** | Access the RStudio IDE from anywhere via a web browser | + | +
x | View Python data, publish and knit in Python and share objects with R | + | +
x  | Access the RStudio IDE from anywhere via a web browser | + | +
x  | Author and edit Python code with Jupyter Notebooks, JupyterLab and VSCode | - | +
x  | Easily publish and share Jupyter Notebooks 	 | - | +
**Project Sharing** | Share projects & edit code files simultaneously with others | - | +
**Multiple R Versions** | Run multiple versions of R side-by-side | - | +
**Multiple R and Python Sessions** |  	Run multiple analyses in parallel | - | +
**Load Balancing** | Load balance R sessions across two or more servers | - | +
x  | Ensure high availability using multiple masters | - | +
 **Administrative Dashboard** | Monitor active sessions and their CPU and memory utilization | - | +
x  | Suspend, forcibly terminate, or assume control of any active session | - | +
x  | Ensure high availability using multiple masters | - | +
x  | Review historical usage and server logs | - | +
**Enhanced Security**  | LDAP, Active Directory, Google Accounts and system accounts | - | +
x  | Full support for Pluggable Authentication Modules, Kerberos via PAM, and custom authentication via proxied HTTP header | - | +
x  | Encrypt traffic using SSL and restrict client IP addresses | - | +
**Auditing and Monitoring**  | Monitor server resources (CPU, memory, etc.) on both a per-user and system-wide basis | - | +
x  | Send metrics to external systems with the Graphite/Carbon plaintext protocol | - | +
x  | Health check with configurable output (custom XML, JSON) | - | +
x  | Audit all R console activity by writing input and output to a central location | - | +
**Advanced R Session Management**  | Tailor the version of R, reserve CPU, prioritize scheduling and limit resources by User and Group | - | +
x  | Provision accounts and mount home directories dynamically via the PAM Session API | - | +
x  | Automatically execute per-user profile scripts for database and cluster connectivity | - | +
**Data Connectivity**  | RStudio Professional Drivers are ODBC data connectors that help you connect to some of the most popular databases | - | +
**Launcher**  | Start processes within various systems such as container orchestration platforms | - | +
x  | Submit standalone ad hoc jobs to your compute cluster(s) to run computationally expensive R or Python scripts | - | +
**Tutorial API**  | Automate interactions with the RStudio IDE | - | +
**Remote Sessions**  | Connect to RStudio Workbench directly from RStudio Desktop Pro for more powerful computing resources, freeing up your local system | - | +
	

## Pérennité du service

RStudio Server Pro ainsi que RShiny Server Pro sont des
logiciels sous licence que la TGIR Huma-Num finance et met à
disposition des communautés de recherche SHS depuis décembre 2016.
Actuellement, ils sont commercialisés sous la forme d’une licence
en location par serveur et par application.

- Pour R Studio Server Pro la licence expire fin 2023.
- Pour Shiny Server Pro la licence expire fin 2022.

Nous ne pouvons pas nous engager au-delà de cette période à maintenir ce service.

Dans tous les cas de figure, cela n'impactera pas les comptes
des utilisateurs et les données qui sont indépendantes de l’IDE (Environnement de programmation intégré) permettant d’utiliser R dans un navigateur, mais uniquement le moyen d’y accéder et de lancer des traitements avec R.

