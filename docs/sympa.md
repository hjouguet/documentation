# Sympa : le serveur de listes de discussion

Pour l'animation d'un projet ou d'un groupe de travail, une solution simple et efficace consiste en la création d’une liste de discussion et/ou de diffusion sur un serveur de listes. La TGIR Huma-Num propose à ses utilisateurs le serveur de liste "SYMPA". Le serveur de liste "SYMPA" de la TGIR est réservé aux listes liées aux programmes de recherche et utilisateurs des services de la TGIR uma-Num.

Vous pouvez demander la création de votre liste sur [listes.huma-num.fr](https://listes.huma-num.fr) après authentification avec votre compte [HumanID](https://documentation.huma-num.fr/humanid/). Si vous n'avez pas encore de compte HumanID, veuillez vous en créer un sur [HumanID](https://humanid.huma-num.fr).
Une fois votre demande de liste validée par nos services, vous pourrez y inscrire l’ensemble de vos collègues / collaborateurs afin de partager rapidement des informations et favoriser les échanges d’idées.

!!! Note
    Vos collègues et collaborateurs n'ont pas besoin d'avoir un compte HumanID pour être abonnés à une liste dont vous êtes le propriétaire, sauf dans le cas ou ils souhaitent utiliser l'interface Web [listes.huma-num.fr](https://listes.huma-num.fr)

## Partage de documents sur une liste

Votre liste vous permet aussi de partager simplement des documents dans un petit espace dédié avec vos abonnés uniquement.

Tutoriel :

- Cliquez sur liste des listes
- Choisissez votre liste
- À gauche cliquez sur "Documents partagés"
 - Passez en mode expert pour déposer votre fichier.

Vous disposez maintenant d’un espace en ligne sécurisé où partager des documents avec une liste de personnes spécifiques abonnées à la liste.

!!! Note
    La vocation de cet espace est interne à la liste, il ne s’agit pas de l’utiliser pour stocker des documents et les citer ensuite dans des publications ou billets de carnet de recherche. En effet, les documents déposés n’auront pas d’identifiants pérennes. A la suppression de la liste, les documents ne seront pas conservés dans le serveur (il sera demandé au propriétaire de la liste de les récupérer). Pour de la publication pérenne de jeux de données et/ou de documents d'archives, privilégiez [NAKALA](https://documentation.huma-num.fr/nakala/). Pour des articles ou des ouvrages, privilégiez un dépôt sur [HAL](https://hal.archives-ouvertes.fr) ou encore [Zenodo](https://zenodo.org).
