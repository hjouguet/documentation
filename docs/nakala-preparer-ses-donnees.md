---
lang: fr
---

# Préparer les données pour les déposer dans NAKALA

## Avant de débuter le projet

La plupart des agences de financement demandent aujourd'hui de rédiger un [plan de gestion de données](https://doranum.fr/plan-gestion-donnees-dmp/) (Data Management Plan) qui permet de planifier la gestion des données durant la durée du projet, mais également et surtout lorsque le projet est terminé. 

Cette démarche est importante et doit être mise en oeuvre avant de débuter un projet même si le plan de gestion n'est pas formellement demandé.

Les questions (la liste n'est pas exhaustive) à se poser sont les suivantes;  
-   Comment se fera la collecte ou la création des données nécessaires à mon projet;  
-   Quel sera le volume prévisionnel de ces données;  
-   Avec quels outils ces données seront traitées;  
-   Comment ces données seront organisées (e.g. avec quelle granularité);  
-   Quels formats seront utilisés pour ces données;  
-   Comment seront documentées ces données (e.g. quelles informations doivent être collectées);  
-   Dans quel(s) entrepôt(s) ces données seront entreposées entre autre dans le but les signaler largement via des aggrégateurs (e.g. [ISIDORE](https://isidore.science/));  
-   Quelles licences seront utilisées pour mettre à disposiiton ces données;  
-   Les données comportent-elles des informations à caractère personnel, des informations "sensibles"? (démarches en conformité au RGPD)  
etc.

Des outils d'aide  à la création de plan de gestion de données sont disponibles comme par exemple [DMP OPIDOR](https://dmp.opidor.fr/) proposé par l'[INIST](https://www.inist.fr/).


Des documents de référence maintenus par la CNRS sont également utiles à consulter à cette étape du projet :  
-  [Plan données de la recherche du CNRS](https://www.cnrs.fr/sites/default/files/pdf/Plaquette_PlanDDOR_Nov20.pdf);  
-  [INSHS : Guide pour la recherche](https://www.inshs.cnrs.fr/sites/institut_inshs/files/pdf/Guide_rgpd_2021.pdf)  
Les sciences humaines et sociales et la protection des données à caractère personnel dans le contexte de la science ouverte.  



## Choisir le(s) format(s)

Des formats ouverts et dont la feuille de route d'évolution est connue sont à privilégier à des formats propriétaires.

Nationalement le [CINES](https://www.cines.fr/archivage/), [partenaire de Huma-Num](/partenariat-hn-cines)  pour la préservation à long terme, effectue une veille sur les formats. 
Une liste de formats de qualité associés lorsque cela est possible à un outil de validation est disponible à cette adresse <http://facile.cines.fr>  

Une autre ressource intéressante est la [matrice des risques](https://github.com/usnationalarchives/digital-preservation/tree/master/Digital_Preservation_Risk_Matrix) associés à l'utilisation de formats maintenue par les archives nationales américaines ([NARA](https://www.archives.gov/)). Vous y trouverez pour la plupart des formats de fichiers un indicateur de risque (Low, Moderate, High) ainsi qu'une indication des formats "acceptables" ou "préférés" qui vous permettront de guider vos choix. 

En ce qui concerne les outils associés, vous pouvez consulter le [répertoire](https://coptr.digipres.org/index.php/Tools_Grid) constitué par le projet [COPTER (Community Owned digital Preservation Tool Registry)](https://coptr.digipres.org/index.php/About_COPTR).

Par ailleurs, il peut être utile de suivre les travaux du [groupe de travail national](https://www.association-aristote.fr/cellule-format/) du groupe PIN (Préservation de l'Information Nuémrique)sur les questions de format composé d'experts de nombreuses institutions et qui a été créé sous l'égide de l'association Aristote. 



## Organisation des données 

### Granularité du dépôt

NAKALA permet d'effectuer des dépôts contenant un ou plusieurs fichiers et ces dépôts peuvent être organisés en collections (Cf. la [documentation](https://documentation.huma-num.fr/nakala/) de NAKALA).  

Il est donc nécessaire de se poser la question de l'organisation à prévoir pour mettre à disposition les données de la manière la plus compréhensible en donnant le maximum d'informations sur le contexte de production de ces données ainsi que les traitements qui leur ont été associés. Généralement, une granularité  plus fine (e.g. un fichier par dépôt) permet une description plus poussée mais les regroupements peuvent intellectuellement faire sens comme par exemple associer dans un même dépôt toutes les pages numérisées d'un ouvrage. 

### Plan de nommage

Dans tous les cas, pour une gestion efficace des données même bien en amont du dépôt dans NAKALA, il est utile d'élaborer un plan de nommage cohérent qui permettra d'organiser les fichiers de données en les regroupant en dossiers. 
Cette réflexion permettra, entre autres gains, d'éviter la duplication ou la perte de données.

Quelques bonnes pratiques sont à utiliser pour nommer les fichiers et les dossiers :  
-  adoption d'une taille raisonnable pour les noms (i.e. moins 30 caractères);
-  emploi de caractères standard en excluant les diacritiques (e.g. "ç"), les caractères spéciaux (e.g. "&"), les espaces etc.  
-  utilisation des titres signifiants et adopter une cohérence dans le nommage (e.g. ajout de la date dans le nom du fichier pour pouvoir effectuer des tris).

### Plan de classement

Si la phase de documentation des données est effectuée après leur collecte, un plan de classement simple et arborescent permet de structurer celles-ci dans une structure facilitant, la recherche, les contrôles et les traitements. Ces structures peuvent aller d'un simple chrono à la reproduction d'organisations géographiques ou thématiques suivant la nature du projet.

## Documenter les données 

Une bonne description des données par des métadonnées est fondamentale pour leur mise à disposition, il s'agit d'un travail complémentaire à l'organisation des données décrite à la section précédente. De même que précédemment, il sera utile d'élaborer en amont et de mettre en oeuvre des règles de description cohérentes (e.g. titres, dates, auteurs, mots clefs etc.) pour tous les données du projet.

NAKALA utilise le modèle Dublin Core pour décrire les données, des indications concernant l'utilisation de ce modèle se trouvent dans le [guide de description](/nakala-guide-de-description/). 
