# Machines virtuelles

## Conditions Générales d'Utilisation (CGU)

Voir dans cette documentation, [les CGU](cgu.md).

## Référence à Huma-Num

Vous êtes invités à mentionner sur la page d'accueil de votre site Web
le fait que vous êtes hébergé par la TGIR Huma-Num.

Vous trouverez un choix
d'images sur [www.huma-num.fr/supports-communication/](https://www.huma-num.fr/supports-communication/).


## Annuaire des sites hébergés

Un annuaire des sites Web hébergés est consultable sur [www.huma-num.fr/annuaire-des-sites-web](https://www.huma-num.fr/annuaire-des-sites-web).

Chaque gestionnaire de site est invité à demander l'inscription et la modification de son site
dans cet annuaire.


## Systèmes disponibles

Les systèmes disponibles sont: 

-   Debian 11

-   Ubuntu 20

-   CentOS 8

## Ouverture des ports

Le port `22 - SSH` est ouvert uniquement depuis la France.

Les ports `80 - HTTP` et `443 - HTTPS` sont ouverts sur tout Internet.

Si vous avez besoin d'accéder à d'autres ports dans votre VM,
merci d'utiliser un mécanisme de redirection via Apache ou Nginx.

## URL

Aucune URL sur le domaine `in2p3.fr` ne sont autorisées.
Vous devez impérativement utiliser le domaine `huma-num.fr`
ou un domaine de votre choix pour publier un site Web.

La génération d'un certificat SSL par la boite à outils _Let's Encrypt_
est possible et à votre charge.

## Mises à jour du système et des paquets

Nous opérons pour vous une mise à jour du système et de tous les paquets
logiciels plusieurs fois par semaine.

Si vous souhaitez que certains paquets ne soient pas mis à jour par ces
vagues automatiques, il convient de configurer les outils `yum` ou `apt`
en conséquence. Nous demander de l'aide au besoin.

## Synchronisation de l'horloge

Les machines virtuelles sont synchronisées au niveau de leur horloge par
le protocole NTP sur l'adresse définie dans `/etc/ntp.conf`.

## Envoi de messages électroniques

Vous disposez d'un serveur SMTP permettant l'envoi de messages à
l'adresse `relay.huma-num.fr`
(ceci sans chiffrement, sans authentification et sur le port 25 traditionnel).

## Journaux

Les journaux émis par le système sont stockés jour par jour et conservés
300 jours. Cf le dossier `/var/log`.

Ils sont également émis vers un service central de journaux.

Vous pouvez étendre l'usage du système de rotation des logs à vos
services applicatifs, via l'outil `logrotate`.  
Cf le dossier `/etc/logrotate.d`

## Messages émis par le système

Vous pouvez recevoir si vous le souhaitez les messages émis par le système.
Pour cela, il suffit d'ajouter, en tant que root, l'adresse
de votre choix dans les dernières lignes du fichier `/etc/aliases`
 et de taper la commande `newaliases`.

## Sauvegardes des machines virtuelles et de leur contenu

Quatre fonctionnalités de sauvegarde sont mis en place.

### 1. Sauvegarde de la configuration

Un premier niveau de sauvegarde de la configuration de la VM (c'est-à-dire le dossier `/etc`) est réalisé en interne au sein de la VM via le script `/etc/cron.daily/sauveMachine` appelé chaque jour, et qui produit le contenu du dossier `/opt/backup/host`.

Vous pouvez donc récupérer facilement un fichier récent contenu dans `/etc`.

### 2. Sauvegarde par TSM

Un deuxième niveau de sauvegarde est réalisé par le logiciel TSM
(Tivoli) mis à disposition par le CC-IN2P3.

Celui-ci réalise une sauvegarde journalière incrémentale de l'ensemble des dossiers indiqués

- dans le fichier `/var/spool/tsm/etc/dsm.sys`  par la ligne

```bash
DOMAIN /etc /home /usr/local /opt /root /var/spool /cron /var/spool/tsm /var/log
```

IMPORTANT: si vous avez mis des données importantes dans un autre dossier,
il faut l'ajouter à la config TSM dans deux fichiers :

- `/var/spool/tsm/etc/dsm.sys`
- `/var/spool/tsm/etc/inclexcl.def`

Nous demander de l'aide pour le faire avec vous.

Le fichier produit chaque nuit par TSM est `/tmp/dsmc.incremental.local.log`

L'accès à l'outil TSM se fait par `dsmc`.

La documentation complète est consultable sur [la documentation de l'IN2p3](https://doc.cc.in2p3.fr/fr/Data-storage/mass-storage/backup.html#utilisation).

Un exemple : pour restaurer le fichier `/var/spool/tsm/etc/dsm.sys` à sa dernière version dans le fichier `/tmp/dsm.sys.restored` :

```bash
dsmc restore /var/spool/tsm/etc/dsm.sys /tmp/dsm.sys.restored
```

### 3. Snapshots NetApp

Un troisième niveau de sauvegarde est assuré par le mécanisme de
snapshots du serveur de fichiers NetApp qui héberge les disques virtuels.

Celui prend une "photo" de ces disques virtuels

- 6 fois par jour
-  chaque jour pendant une semaine
-  chaque semaine pendant un mois.

En cas de besoin (fausse manipulation, corruption, ...), il convient de nous demander
de relancer la machine virtuelle sur l'une de ces "photos".

### 4. Recopie sur un espace physiquement distinct

Un dernier niveau de sauvegarde est assuré par la recopie sur un autre
espace de stockage physique des disques virtuels des VM, ceci une fois par mois.

### IMPORTANT : aucune sauvegarde de base de données n'est pré-définie !

Aussi si vous avez mis en oeuvre un moteur

- MySQL
- PostgreSQL
- autour de XML

il vous FAUT IMPÉRATIVEMENT agir :

1. nous demander un exemple de script pour réaliser une sauvegarde dans votre VM, si le volume de vos données est faible.
2. sinon, nous demander de réaliser une sauvegarde externe, en nous donnant accès à un compte ayant les droits nécessaires.


## Autres services disponibles

### Supervision et métrologie

Vous pouvez accéder à notre console de supervision et métrologie (basée
sur Shinken, dérivé de Nagios) qui vous permet d'observer les
événements concernant votre VM et les incidents sur divers processus.

Il convient de nous demander l'accès à ce service pour consulter :

-   [les problèmes en cours](https://kvm-cyclope.huma-num.fr/problems)
-   [tout ce qui est supervisé pour vous](https://kvm-cyclope.huma-num.fr/all)

Nous vous invitons fortement aussi à nous demander d'ajouter, dans
cet outil de supervision, les services que vous avez installés dans
votre VM (serveur Web, serveur de base de données, serveur d'indexation, ...)

Plus globalement, on peut superviser tout processus ou toute
grandeur scalaire, et cela par un programme écrit en n'importe quel
langage de programmation. Aussi si vous avez envie de vérifier ou
suivre dans le temps des grandeurs, n'hésitez pas à nous préciser
votre besoin.

Des graphiques permettant de visualiser dans le temps les grandeurs
scalaires (charge CPU ou mémoire, remplissage des disques, bande
passante, etc), ceci à différentes échelles de temps (sur 4 heures,
1 jour, 1 semaine, 1 mois, 1 an) sont disponibles. Ces graphiques
sont "zoomables" pour regarder plus finement un intervalle de
temps plus restreint.

### Annuaire LDAP

L'accès au service de supervision nécessite une authentification via.
notre annuaire LDAP ([annuaire.huma-num.fr/](https://annuaire.huma-num.fr/)).

Cet annuaire permet une délégation de gestion de branches. Si vous avez
besoin de gérer un certain nombre de comptes (système ou applicatifs), vous
pourriez avoir bénéfice à utiliser l'interface Web de gestion de cet
annuaire, qui permet simplement de gérer des comptes et des groupes, et
à chaque personne, de changer son mot de passe et de nombreuses informations personnelles.

### Statistiques Web

Nous proposons un service de statistiques Web basé sur l'outil libre et interne Matomo.

Il convient d'en demander l'accès depuis votre compte [humanid.huma-num.fr](https://humanid.huma-num.fr/).

### Forge logicielle

Nous proposons également un service de force logicielle avec l'outil GitLab permettant d'héberger des projets.  

Il est accessible sur [gitlab.huma-num.fr](https://gitlab.huma-num.fr/).

Il convient d'en demander l'accès depuis votre compte [humanid.huma-num.fr](https://humanid.huma-num.fr/).

## Support

Toute demande concernant ce service doit impérativement être envoyée à l'adresse [assistance@huma-num.fr](mailto:assistance@huma-num.fr)
