---
title: Heurist
lang: fr
description: Heurist, un service tiers hébergé par Huma-Num et lien vers sa documentation.
---

# Heurist

!!! attention "À savoir"
    Les services tiers sont des outils et des plateformes dont la conception,
    la maintenance et la documentation sont assurés par  d'autres instances ou
    organisations que la TGIR Huma-Num. La TGIR Huma-Num met à disposition des
    machines virtuelles hébergées sur son infrastructure pour assurer l'accessibilité
    de ces outils et soutenir leur usage par la communauté SHS.


Heurist est un logiciel open source qui permet l’élaboration de bases de données relationnelles richement structurées. Par simple navigateur web et sans installation ni programmation, Heurist permet de créer une base de données MySQL[^1].

[^1]: Source [MATE-SHS](https://mate-shs.cnrs.fr/actions/tutomate/tuto26-heurist-ian-johnson/)

Heurist a été conçu par [Ian Johnson](http://sydney.academia.edu/Johnson), directeur fondateur de Arts eResearch (anciennement Archaeological Computing Laboratory) de l'Université de Sydney, où il a été développé de septembre 2005 à décembre 2013. Heurist continue d'être développé et soutenu au sein de la faculté des arts et des sciences sociales et du portefeuille de recherche de l'université, sous la direction de Ian Johnson.

!!! note
    - Voir l'instance _Heurist_ hébergée par la TGIR Huma-Num : [heurist.huma-num.fr](https://heurist.huma-num.fr)
    - Voir [la documentation d'Heurist](https://heuristnetwork.org/)
    - Participer à la liste des utilisateurs de _Heurist_ : [heurist-utilisateurs@groupes.renater.fr](mailto:heurist-utilisateurs@groupes.renater.fr)
